/// <reference types = "Cypress" />

let username="JohnDoe";
let usernameupdate ="JohnDoerrr"
describe('Create New User', () => {
   

    it('post user request', () => {
        let id=1;
        let firstName="John";
        let lastName="Doe";
        let email="JOhnDoeasik@Gmail.com";
        let password ="123qweasad";
        let phone="082193328238";
        let userStatus=0;

        cy.request({
            method: 'POST',
            url: 'https://petstore.swagger.io/v2/user',
            body: {
                "id": id,
                "username": username,
                "firstName": firstName,
                "lastName": lastName,
                "email": email,
                "password": password,
                "phone": phone,
                "userStatus": userStatus
            }
        }).then((response) => {
            expect(response.status).to.eq(200)
            expect(response.body.type).to.eq("unknown")
            expect(response.body.message).not.equal("")
        })
})
})

describe('Update User Existing ', () => {
   

    it('Update user request', () => {

        cy.request({
            method: 'PUT',
            url: 'https://petstore.swagger.io/v2/user/' + username,
            body: 
                {
                    "id": 0,
                    "username": usernameupdate,
                    "firstName": "string",
                    "lastName": "string",
                    "email": "string",
                    "password": "string",
                    "phone": "string",
                    "userStatus": 0
                  }
        }).then((response) => {
            expect(response.status).to.eq(200)
            expect(response.body.code).to.eq(200)
            expect(response.body.type).to.eq("unknown")
        })
})
})


describe('Delete User Existing ', () => {
   

    it('Delete user ', () => {

        cy.request({
            method: 'DELETE',
            url: 'https://petstore.swagger.io/v2/user/' + username,
        }).then((response) => {
            expect(response.status).to.eq(200)
            expect(response.body.code).to.eq(200)
            expect(response.body.type).to.eq("unknown")
            expect(response.body.message).to.eq(username)
            // expect(response.body.type).to.eq("unknown")
            // expect(response.body.message).not.equal("")
        })
})
})